/*  Nama    :Imam Rizki Saputra
    Kelas   :IF-1B
    NIM     :301230013
*/

#include <iostream>

using namespace std;
int main()
{
    int N;
    cout << "masukan N: ";
    cin >> N;
    for (int i = 0; i < N; i++)
    {
        int factorial = 1;
        for (int j = N; j >= i; j--)
        {
            cout << " ";
        }
        for (int k = 0; k <= i; k++)
        {
            cout << " " << factorial;
            factorial = factorial * (i - k) / (k + 1);
        }
        cout << endl;
    }
}

