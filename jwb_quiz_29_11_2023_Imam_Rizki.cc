/*  Nama    :Imam Rizki Saputra
    Kelas   :IF-1B
    NIM     :301230013
*/

#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
    system("clear");

    int N = 0;
    float Data = 0.0, Rata = 0.0, Total = 0.0;

    int i = 1;
    cout << " Masukkan berapa data yang di inginkan : ";
    cin >> N;
    Total = 0;
    do
    {
        cout << " Masukkan Data ke " << i << " : ";
        i++;
        cin >> Data;
        Total += Data;
    }while (i <= N);

    Rata = Total / N;
    cout << "Banyaknya Data : " << N << endl;
    cout << "Total Nilai Data : " <<  setprecision(2) << Total << endl;
    cout << "Rata-rata nilai Data : " << setprecision(2) << Rata << endl;

    return 0;

}






