/*  Nama    :Imam Rizki Saputra
    Kelas   :IF-1B
    NIM     :301230013
*/

#include <iostream>

using namespace std;

#include "myheader.cpp"
int main() {
    char pilihan;
    int bilangan1, bilangan2, hasil;

    menu();
    proses(pilihan, bilangan1, bilangan2, hasil);
    hitungDanTampilkan(pilihan, bilangan1, bilangan2, hasil);

    return 0;
}
